﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Students
{
    /// <summary>
    /// Klasa reprezentująca pojedyńczego studenta w systemie.
    /// </summary>
    [DataContract]
    public class Student
    {
        public Student()
        {
            this.Lectures = new List<Lecture>();
        }

        /// <summary>
        /// Numer indeksu(indetyfiaktor studenta na uczelni).
        /// </summary>
        [DataMember]
        public int Album { get; set; }
        /// <summary>
        /// Imię studenta.
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }
        /// <summary>
        /// Nazwisko studenta.
        /// </summary>
        [DataMember]
        public string LastName { get; set; }
        /// <summary>
        /// Data urodzenia studenta.
        /// </summary>
        [DataMember]
        public DateTime BirthDate { get; set; }
        /// <summary>
        /// Najlepszy przedmiot.
        /// </summary>
        public string BestLecture
        {
            get
            {
                if (this.Lectures.Count == 0) return "";
                Lecture bestLecture = this.Lectures.OrderByDescending(x => x.Grade).First();
                return bestLecture.Name;
            }
        }
        /// <summary>
        /// Średnia ocen z przedmiotów.
        /// </summary>
        [DataMember]
        public double AverageRate
        {
            get
            {
                return this.Lectures.Count == 0 ? 0 : this.Lectures.Average(x => x.Grade);
            }
            // keep WCF happy
            set { }
        }

        /// <summary>
        /// Lista przedmiotów studenta.
        /// </summary>

        [DataMember]
        public List<Lecture> Lectures { get; set; }

        public override string ToString()
            => $"{FirstName} {LastName}";
    }
}
