﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Students
{
    [DataContract]
    public class Lecture
    {
        /// <summary>
        /// Nazwa przedmiotu
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// Ocena za przedmiot.
        /// </summary>
        [DataMember]
        public double Grade { get; set; }

    }
}
