﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Students.Wcf
{
    public class StudentService : IStudentService
    {
        private readonly XmlDatabase db = new XmlDatabase(@"C: \Users\Marek\OneDrive\Studia\Programowanie\Studenci_baza.dat");

        public List<Student> All()
        {
            return db.Load();
        }

        public void Create(Student student)
        {
            var students = db.Load();

            int newAlbum = 1;
            if (students.Count > 0)
                newAlbum = students.Max(x => x.Album) + 1;

            student.Album = newAlbum;
            students.Add(student);
            db.Save(students);
        }

        public void Delete(int album)
        {
            var students = db.Load();
            var student = students.FirstOrDefault(s => s.Album == album);
            if (student != null)
            {
                students.Remove(student);
                db.Save(students);
            }
        }

        public void Update(int oldAlbum, Student student)
        {
            var students = db.Load();
            var studentToRemove = students.FirstOrDefault(s => s.Album == oldAlbum);
            if (student != null)
            {
                students.Remove(studentToRemove);
            }
            students.Add(student);
            db.Save(students);
        }

        public Student Get(int album)
        {
            var students = db.Load();
            return students.FirstOrDefault(s => s.Album == album);
        }
    }
}
