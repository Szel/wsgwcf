﻿using Students;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Students.Wcf
{
    public class XmlDatabase
    {
        private readonly string fileName;

        public XmlDatabase(string dbFileName)
        {
            fileName = dbFileName;
        }

        public List<Student> Load()
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                XmlSerializer xser = new XmlSerializer(typeof(List<Student>));
                return (List<Student>)xser.Deserialize(sr);
            }
        }

        public void Save(List<Student> students)
        {
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                XmlSerializer xser = new XmlSerializer(typeof(List<Student>));
                xser.Serialize(sw, students);
            }
        }
    }
}
