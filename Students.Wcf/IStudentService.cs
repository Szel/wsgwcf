﻿using Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Students.Wcf
{
    [ServiceContract]
    public interface IStudentService
    {
        [OperationContract]
        Student Get(int album);

        [OperationContract]
        List<Student> All();

        [OperationContract]
        void Create(Student student);

        [OperationContract]
        void Update(int oldAlbum, Student student);

        [OperationContract]
        void Delete(int album);
    }
}
