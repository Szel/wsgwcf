﻿using StudentBase.StudentServiceRef;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentBase
{
    public partial class LectureForm : Form
    {
        public Lecture NewLecture { get; private set; }
        public LectureForm()
        {
            InitializeComponent();
            NewLecture = new Lecture();
        }

        private void btnAddLecture_Click(object sender, EventArgs e)
        {
            NewLecture.Name = tbLecture.Text;
            NewLecture.Grade = (double)nudGrade.Value;           
        }
    }
}
