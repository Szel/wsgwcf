﻿using StudentBase.StudentServiceRef;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace StudentBase
{
    public partial class MainForm : Form
    {
        private List<Student> baza = new List<Student>();
        private StudentServiceClient service = new StudentServiceClient();

        public MainForm()
        {
            InitializeComponent();
            RefreshStudents();

        }

        private void RefreshStudents()
        {
            baza = service.All();
            bsStudents.DataSource = baza;
            bsStudents.ResetBindings(false);
        }

        private void CreateStudent_Click(object sender, EventArgs e)
        {
            NewStudentForm dialog = new NewStudentForm(new Student());
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                service.Create(dialog.NewStudent);
                RefreshStudents();
            }
        }

        private void oldestStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var student = baza.OrderBy(s => s.BirthDate).FirstOrDefault();
            if (student == null)
            {
                MessageBox.Show("Brak studentów");
                return;
            }

            MessageBox.Show("Najstarszy student: " + StudentFriendlyName(student));
        }

        private void youngestStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var student = baza.OrderByDescending(s => s.BirthDate).FirstOrDefault();
            if (student == null)
            {
                MessageBox.Show("Brak studentów");
                return;
            }

            MessageBox.Show("Najmłodszy student: " + StudentFriendlyName(student));
        }

        private void bestAvarageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var student = baza.OrderByDescending(s => s.AverageRate).FirstOrDefault();
            if (student == null)
            {
                MessageBox.Show("Brak studentów");
                return;
            }

            MessageBox.Show("Student z najlepszą średnią: " + StudentFriendlyName(student));
        }

        private void toRemoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var students = baza.Where(s => s.Lectures.Any(l => l.Grade == 2));
            bsStudents.DataSource = students;
        }

        private void phisicsTrublesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var students = baza.Where(s => s.Lectures.Any(l => l.Name == "Fizyka" && l.Grade <= 3));
            bsStudents.DataSource = students;
        }

        private void allStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshStudents();
        }

        private string StudentFriendlyName(Student student)
            => $"{student.FirstName} {student.LastName}";

        private void dgvStudents_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var student = (Student)dgvStudents.Rows[e.RowIndex].DataBoundItem;
            var oldAlbum = student.Album;
            var form = new NewStudentForm(student);
            if (form.ShowDialog() == DialogResult.OK)
            {
                service.Update(oldAlbum, student);
            }
            RefreshStudents();
        }
    }
}
