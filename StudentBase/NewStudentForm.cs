﻿using StudentBase.StudentServiceRef;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace StudentBase
{
    public partial class NewStudentForm : Form
    {
        public Student NewStudent { get; set; }

        public NewStudentForm(Student student)
        {
            InitializeComponent();
            NewStudent = student;
            bslecture.DataSource = student.Lectures;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (NewStudent == null)
                NewStudent = new Student();

            NewStudent.FirstName = tbFirstName.Text;
            NewStudent.LastName = tbLastName.Text;
            NewStudent.BirthDate = dtpBirthdate.Value;
        }

        private void btnAddLecture_Click(object sender, EventArgs e)
        {
            LectureForm dialog = new LectureForm();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                bslecture.Add(dialog.NewLecture);
            }
        }

        private void dtLecture_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            bslecture.RemoveAt(e.RowIndex);
        }
    }
}
