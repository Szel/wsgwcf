﻿namespace StudentBase
{
    partial class MainForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnu = new System.Windows.Forms.MenuStrip();
            this.mniFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mniFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this.statystykaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wszyscyStudenciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.najlepszaŚredniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.najstarszyStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.najmłodszyStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doUsunięciaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kłopotyZFizykiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zNajwyższToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mniStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvStudents = new System.Windows.Forms.DataGridView();
            this.bsStudents = new System.Windows.Forms.BindingSource(this.components);
            this.ofdStudent = new System.Windows.Forms.OpenFileDialog();
            this.sfdStudent = new System.Windows.Forms.SaveFileDialog();
            this.albumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AverageRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BestLecture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.mnu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsStudents)).BeginInit();
            this.SuspendLayout();
            // 
            // mnu
            // 
            this.mnu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniFile,
            this.statystykaToolStripMenuItem,
            this.studentToolStripMenuItem});
            this.mnu.Location = new System.Drawing.Point(0, 0);
            this.mnu.Name = "mnu";
            this.mnu.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.mnu.Size = new System.Drawing.Size(1297, 35);
            this.mnu.TabIndex = 0;
            this.mnu.Text = "menuStrip1";
            // 
            // mniFile
            // 
            this.mniFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniFileClose});
            this.mniFile.Name = "mniFile";
            this.mniFile.Size = new System.Drawing.Size(51, 29);
            this.mniFile.Text = "&Plik";
            // 
            // mniFileClose
            // 
            this.mniFileClose.Name = "mniFileClose";
            this.mniFileClose.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.mniFileClose.Size = new System.Drawing.Size(268, 30);
            this.mniFileClose.Text = "Zam&knij";
            // 
            // statystykaToolStripMenuItem
            // 
            this.statystykaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wszyscyStudenciToolStripMenuItem,
            this.najlepszaŚredniaToolStripMenuItem,
            this.najstarszyStudentToolStripMenuItem,
            this.najmłodszyStudentToolStripMenuItem,
            this.doUsunięciaToolStripMenuItem,
            this.kłopotyZFizykiToolStripMenuItem,
            this.zNajwyższToolStripMenuItem});
            this.statystykaToolStripMenuItem.Name = "statystykaToolStripMenuItem";
            this.statystykaToolStripMenuItem.Size = new System.Drawing.Size(104, 29);
            this.statystykaToolStripMenuItem.Text = "Statystyka";
            // 
            // wszyscyStudenciToolStripMenuItem
            // 
            this.wszyscyStudenciToolStripMenuItem.Name = "wszyscyStudenciToolStripMenuItem";
            this.wszyscyStudenciToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.wszyscyStudenciToolStripMenuItem.Text = "Wszyscy studenci";
            this.wszyscyStudenciToolStripMenuItem.Click += new System.EventHandler(this.allStudentsToolStripMenuItem_Click);
            // 
            // najlepszaŚredniaToolStripMenuItem
            // 
            this.najlepszaŚredniaToolStripMenuItem.Name = "najlepszaŚredniaToolStripMenuItem";
            this.najlepszaŚredniaToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.najlepszaŚredniaToolStripMenuItem.Text = "Najlepsza Średnia";
            this.najlepszaŚredniaToolStripMenuItem.Click += new System.EventHandler(this.bestAvarageToolStripMenuItem_Click);
            // 
            // najstarszyStudentToolStripMenuItem
            // 
            this.najstarszyStudentToolStripMenuItem.Name = "najstarszyStudentToolStripMenuItem";
            this.najstarszyStudentToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.najstarszyStudentToolStripMenuItem.Text = "Najstarszy student";
            this.najstarszyStudentToolStripMenuItem.Click += new System.EventHandler(this.oldestStudentToolStripMenuItem_Click);
            // 
            // najmłodszyStudentToolStripMenuItem
            // 
            this.najmłodszyStudentToolStripMenuItem.Name = "najmłodszyStudentToolStripMenuItem";
            this.najmłodszyStudentToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.najmłodszyStudentToolStripMenuItem.Text = "Najmłodszy student";
            this.najmłodszyStudentToolStripMenuItem.Click += new System.EventHandler(this.youngestStudentToolStripMenuItem_Click);
            // 
            // doUsunięciaToolStripMenuItem
            // 
            this.doUsunięciaToolStripMenuItem.Name = "doUsunięciaToolStripMenuItem";
            this.doUsunięciaToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.doUsunięciaToolStripMenuItem.Text = "Do usunięcia";
            this.doUsunięciaToolStripMenuItem.Click += new System.EventHandler(this.toRemoveToolStripMenuItem_Click);
            // 
            // kłopotyZFizykiToolStripMenuItem
            // 
            this.kłopotyZFizykiToolStripMenuItem.Name = "kłopotyZFizykiToolStripMenuItem";
            this.kłopotyZFizykiToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.kłopotyZFizykiToolStripMenuItem.Text = "Kłopoty z fizyki";
            this.kłopotyZFizykiToolStripMenuItem.Click += new System.EventHandler(this.phisicsTrublesToolStripMenuItem_Click);
            // 
            // zNajwyższToolStripMenuItem
            // 
            this.zNajwyższToolStripMenuItem.Name = "zNajwyższToolStripMenuItem";
            this.zNajwyższToolStripMenuItem.Size = new System.Drawing.Size(294, 30);
            this.zNajwyższToolStripMenuItem.Text = "Z najwyższą średnią ocen";
            this.zNajwyższToolStripMenuItem.Click += new System.EventHandler(this.bestAvarageToolStripMenuItem_Click);
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniStudent});
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(85, 29);
            this.studentToolStripMenuItem.Text = "&Student";
            // 
            // mniStudent
            // 
            this.mniStudent.Name = "mniStudent";
            this.mniStudent.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.mniStudent.Size = new System.Drawing.Size(218, 30);
            this.mniStudent.Text = "&Nowy...";
            this.mniStudent.Click += new System.EventHandler(this.CreateStudent_Click);
            // 
            // dgvStudents
            // 
            this.dgvStudents.AllowUserToAddRows = false;
            this.dgvStudents.AllowUserToDeleteRows = false;
            this.dgvStudents.AutoGenerateColumns = false;
            this.dgvStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.albumDataGridViewTextBoxColumn,
            this.firstNameDataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.birthDateDataGridViewTextBoxColumn,
            this.AverageRate,
            this.BestLecture,
            this.Edit});
            this.dgvStudents.DataSource = this.bsStudents;
            this.dgvStudents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStudents.Location = new System.Drawing.Point(0, 35);
            this.dgvStudents.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvStudents.Name = "dgvStudents";
            this.dgvStudents.ReadOnly = true;
            this.dgvStudents.Size = new System.Drawing.Size(1297, 410);
            this.dgvStudents.TabIndex = 1;
            this.dgvStudents.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStudents_CellDoubleClick);
            // 
            // bsStudents
            // 
            this.bsStudents.DataSource = typeof(StudentBase.StudentServiceRef.Student);
            // 
            // ofdStudent
            // 
            this.ofdStudent.FileName = "openFileDialog1";
            this.ofdStudent.Filter = "Baza Danych Studentów | *dat";
            // 
            // albumDataGridViewTextBoxColumn
            // 
            this.albumDataGridViewTextBoxColumn.DataPropertyName = "Album";
            this.albumDataGridViewTextBoxColumn.HeaderText = "Album";
            this.albumDataGridViewTextBoxColumn.Name = "albumDataGridViewTextBoxColumn";
            this.albumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "FirstName";
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "Imię";
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            this.firstNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // birthDateDataGridViewTextBoxColumn
            // 
            this.birthDateDataGridViewTextBoxColumn.DataPropertyName = "BirthDate";
            this.birthDateDataGridViewTextBoxColumn.HeaderText = "Data urodzenia";
            this.birthDateDataGridViewTextBoxColumn.Name = "birthDateDataGridViewTextBoxColumn";
            this.birthDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.birthDateDataGridViewTextBoxColumn.Width = 150;
            // 
            // AverageRate
            // 
            this.AverageRate.DataPropertyName = "AverageRate";
            this.AverageRate.HeaderText = "Średnia ocen";
            this.AverageRate.Name = "AverageRate";
            this.AverageRate.ReadOnly = true;
            // 
            // BestLecture
            // 
            this.BestLecture.DataPropertyName = "BestLecture";
            this.BestLecture.HeaderText = "Najlepszy przedmiot";
            this.BestLecture.Name = "BestLecture";
            this.BestLecture.ReadOnly = true;
            this.BestLecture.Width = 150;
            // 
            // Edit
            // 
            this.Edit.HeaderText = "Edytuj";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1297, 445);
            this.Controls.Add(this.dgvStudents);
            this.Controls.Add(this.mnu);
            this.MainMenuStrip = this.mnu;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "Baza Danych Studentów";
            this.mnu.ResumeLayout(false);
            this.mnu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsStudents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnu;
        private System.Windows.Forms.ToolStripMenuItem mniFile;
        private System.Windows.Forms.ToolStripMenuItem mniFileClose;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mniStudent;
        private System.Windows.Forms.DataGridView dgvStudents;
        
        private System.Windows.Forms.BindingSource bsStudents;
        private System.Windows.Forms.OpenFileDialog ofdStudent;
        private System.Windows.Forms.SaveFileDialog sfdStudent;
        private System.Windows.Forms.ToolStripMenuItem statystykaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem najlepszaŚredniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem najstarszyStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem najmłodszyStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doUsunięciaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kłopotyZFizykiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zNajwyższToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wszyscyStudenciToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn albumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AverageRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BestLecture;
        private System.Windows.Forms.DataGridViewButtonColumn Edit;
    }
}

