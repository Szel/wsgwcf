﻿namespace StudentBase
{
    partial class LectureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLecture = new System.Windows.Forms.Label();
            this.lbGrade = new System.Windows.Forms.Label();
            this.tbLecture = new System.Windows.Forms.TextBox();
            this.btnAddLecture = new System.Windows.Forms.Button();
            this.btn = new System.Windows.Forms.Button();
            this.nudGrade = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // lbLecture
            // 
            this.lbLecture.AutoSize = true;
            this.lbLecture.Location = new System.Drawing.Point(13, 13);
            this.lbLecture.Name = "lbLecture";
            this.lbLecture.Size = new System.Drawing.Size(59, 13);
            this.lbLecture.TabIndex = 0;
            this.lbLecture.Text = "Przedmiot :";
            // 
            // lbGrade
            // 
            this.lbGrade.AutoSize = true;
            this.lbGrade.Location = new System.Drawing.Point(13, 30);
            this.lbGrade.Name = "lbGrade";
            this.lbGrade.Size = new System.Drawing.Size(45, 13);
            this.lbGrade.TabIndex = 1;
            this.lbGrade.Text = "Ocena :";
            // 
            // tbLecture
            // 
            this.tbLecture.Location = new System.Drawing.Point(78, 10);
            this.tbLecture.Name = "tbLecture";
            this.tbLecture.Size = new System.Drawing.Size(131, 20);
            this.tbLecture.TabIndex = 2;
            // 
            // btnAddLecture
            // 
            this.btnAddLecture.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAddLecture.Location = new System.Drawing.Point(14, 61);
            this.btnAddLecture.Name = "btnAddLecture";
            this.btnAddLecture.Size = new System.Drawing.Size(75, 23);
            this.btnAddLecture.TabIndex = 4;
            this.btnAddLecture.Text = "Potwierdź";
            this.btnAddLecture.UseVisualStyleBackColor = true;
            this.btnAddLecture.Click += new System.EventHandler(this.btnAddLecture_Click);
            // 
            // btn
            // 
            this.btn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn.Location = new System.Drawing.Point(134, 61);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(75, 23);
            this.btn.TabIndex = 5;
            this.btn.Text = "Anuluj";
            this.btn.UseVisualStyleBackColor = true;
            // 
            // nudGrade
            // 
            this.nudGrade.DecimalPlaces = 1;
            this.nudGrade.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudGrade.Location = new System.Drawing.Point(78, 30);
            this.nudGrade.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudGrade.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudGrade.Name = "nudGrade";
            this.nudGrade.Size = new System.Drawing.Size(131, 20);
            this.nudGrade.TabIndex = 6;
            this.nudGrade.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // LectureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(215, 101);
            this.Controls.Add(this.nudGrade);
            this.Controls.Add(this.btn);
            this.Controls.Add(this.btnAddLecture);
            this.Controls.Add(this.tbLecture);
            this.Controls.Add(this.lbGrade);
            this.Controls.Add(this.lbLecture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LectureForm";
            this.Text = "Dodaj przedmiot";
            ((System.ComponentModel.ISupportInitialize)(this.nudGrade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbLecture;
        private System.Windows.Forms.Label lbGrade;
        private System.Windows.Forms.TextBox tbLecture;
        private System.Windows.Forms.Button btnAddLecture;
        private System.Windows.Forms.Button btn;
        private System.Windows.Forms.NumericUpDown nudGrade;
    }
}